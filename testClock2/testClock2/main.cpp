#include <SFML/Graphics.hpp>
#include <cmath>
#include <iostream>
#include <array>

using namespace std;

const int CLOCK_CIRCLE_SIZE = 250;
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;
const int CLOCK_CIRCLE_THICKNESS = 2;


struct CircleHands
{
    sf::RectangleShape hourHand;
    sf::RectangleShape minuteHand;
    sf::RectangleShape secondsHand;
    
    void InitOneHand(sf::RectangleShape &hand, sf::Vector2f size, sf::Color color, sf::Vector2f &windowCenter)
    {
        hand.setSize(size);
        hand.setFillColor(color);
        hand.setOrigin(hand.getGlobalBounds().width / 2, hand.getGlobalBounds().height - 25);
        hand.setPosition(windowCenter);
    }
    
    void InitHands(sf::Vector2f &windowCenter)
    {
        InitOneHand(hourHand, sf::Vector2f(5, 180), sf::Color::Black, windowCenter);
        InitOneHand(minuteHand, sf::Vector2f(3, 240), sf::Color::Black, windowCenter);
        InitOneHand(secondsHand, sf::Vector2f(2, 240), sf::Color(255, 0, 0), windowCenter);
    }
};

void InitDots(array<sf::CircleShape, 60> & dot, int clockCircleSize, sf::RenderWindow & window)
{
    for (int i = 0; i < 60; i++)
    {
        const float angle = i * ((2 * M_PI) / 60);
        const sf::Vector2f offset = float(clockCircleSize - 10) * sf::Vector2f{
            cos(angle),
            sin(angle)
        };
        const float radius = (i % 5 == 0) ? 3 : 1;

        dot[i] = sf::CircleShape(radius);
        dot[i].setFillColor(sf::Color::Black);
        dot[i].setOrigin(dot[i].getGlobalBounds().width / 2, dot[i].getGlobalBounds().height / 2);
        dot[i].setPosition(offset + 0.5f * sf::Vector2f(window.getSize()));
    }
}

void InitNumbers(sf::Font & font, sf::Text (&text)[12], sf::RenderWindow & window)
{
    for (int i = 0; i < 12; i++)
    {
        const float angle = (i + 1) * ((float)(2 * M_PI) / 12) - ((float)M_PI / 2);
        const sf::Vector2f offset = float(CLOCK_CIRCLE_SIZE - 35) * sf::Vector2f{
            cos(angle),
            sin(angle)
        };
        
        text[i].setFont(font);
        text[i].setCharacterSize(24);
        text[i].setFillColor(sf::Color::Black);

        text[i].setString(std::to_string(i + 1));
        text[i].setOrigin(text[i].getGlobalBounds().width / 2, text[i].getGlobalBounds().height / 2);
        text[i].setPosition(offset + 0.5f * sf::Vector2f(window.getSize()));
    }
}

sf::Font InitFont()
{
    sf::Font font;
    if (!font.loadFromFile("sansation.ttf"))
    {
        cout << "Error loading font" << endl;
        exit(1);
    }
    else
    {
        return font;
    }
}

sf::CircleShape InitCirleCenter(sf::Vector2f & windowCenter)
{
    sf::CircleShape centerCircle(10);
    
    centerCircle.setPointCount(100);
    centerCircle.setFillColor(sf::Color::Red);
    centerCircle.setOrigin(centerCircle.getGlobalBounds().width / 2, centerCircle.getGlobalBounds().height / 2);
    centerCircle.setPosition(windowCenter);
    
    return centerCircle;
}

struct App
{
    sf::RenderWindow window;
    sf::Vector2f windowCenter;
    sf::CircleShape centerCircle;
    std::array <sf::CircleShape, 60> dot;
    sf::Font font = InitFont();
    sf::Text text[12];
    sf::CircleShape clockCircle;
    CircleHands clockHands;
    
    sf::CircleShape InitClock()
    {
        sf::CircleShape clockCircle(CLOCK_CIRCLE_SIZE);
        clockCircle.setPointCount(100);
        clockCircle.setOutlineThickness(CLOCK_CIRCLE_THICKNESS);
        clockCircle.setOutlineColor(sf::Color::Black);
        clockCircle.setOrigin(clockCircle.getGlobalBounds().width / 2, clockCircle.getGlobalBounds().height / 2);
        clockCircle.setPosition(window.getSize().x / 2 + CLOCK_CIRCLE_THICKNESS, window.getSize().y / 2 + CLOCK_CIRCLE_THICKNESS);
        
        return clockCircle;
    }
    
    void InitWindow()
    {
        sf::ContextSettings settings;
        settings.antialiasingLevel = 8;
        window.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "SFML Analog Clock", sf::Style::Close, settings);
        windowCenter = sf::Vector2f(window.getSize().x / 2.0f, window.getSize().y / 2.0f);
    }
    
    void InitField()
    {
        InitWindow();
        centerCircle = InitCirleCenter(windowCenter);
        clockCircle = InitClock();
        InitDots(dot, CLOCK_CIRCLE_SIZE, window);
        InitNumbers(font, text, window);
    }
    
    void SetupClock()
    {
        InitField();
        
        // Create hour, minute, and seconds hands
        clockHands.InitHands(windowCenter);
    }
    
    void Draw()
    {
        window.draw(clockCircle);
        
        for (int i = 0; i < 60; i++)
        {
            window.draw(dot[i]);
        }
        
        for (int j = 0; j < 12; j++)
        {
            window.draw(text[j]);
        }
        
        window.draw(clockHands.hourHand);
        window.draw(clockHands.minuteHand);
        window.draw(clockHands.secondsHand);
        window.draw(centerCircle);
    }
    
    void Update()
    {
        time_t currentTime = std::time(NULL);
        
        struct tm * ptm = localtime(&currentTime);
        
        clockHands.hourHand.setRotation(ptm->tm_hour * 30 + (ptm->tm_min / 2) );
        clockHands.minuteHand.setRotation(ptm->tm_min * 6 + (ptm->tm_sec / 12) );
        clockHands.secondsHand.setRotation(ptm->tm_sec * 6);
    }
    
    void HandleEvents()
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Window closed: exit
            if (event.type == sf::Event::Closed)
                window.close();
        }
    }
};

int main()
{
    App clock;
    clock.SetupClock();
    while (clock.window.isOpen())
    {
        // Handle events
        clock.HandleEvents();
        
        // Draw all parts of clock
        clock.Update();
        
        // Clear the window
        clock.window.clear(sf::Color::White);
        
        clock.Draw();
        
        // Display things on screen
        clock.window.display();
    }
    
    return EXIT_SUCCESS;
}
