#include <iostream>
#include <cmath>

// This program takes max jump height from input and prints
// jump height for every time point with step 0.1 seconds.
// Program should print all time points when height is min and max.

using namespace std;

const float GRAVITY = 9.8f;

void countCurrentHeightAndTime(const float maxHeightTime, float currTime)
{
    // T - time point when height is at maximum.
    // t - current time point
    // v(t) == v0 - g * t
    // v0 = g * T
    // s(t) == v0 * t - 0.5 * g * t * t
    const float V0 = GRAVITY * maxHeightTime;
    float currentHeight = V0 * currTime - 0.5 * GRAVITY * currTime * currTime;
    cout << "time= " << currTime << " height= " << currentHeight << endl;
}

void printCurrentHeightAndTime(const float maxHeightTime)
{
    const float currTimeInterval = 0.1f;
    float currTime = 0;
    while (currTime <= maxHeightTime * 2)
    {
        countCurrentHeightAndTime(maxHeightTime, currTime);
        currTime += currTimeInterval;
    }
}

void checkForBadInputData(int inputData)
{
    if (inputData <= 0)
    {
        cout << endl << "Expected floating-point or negative number." << endl;
        exit(1);
    }
}

float countMaxHeightTime(int jumpHeight)
{
    return sqrt((jumpHeight * 2) / GRAVITY);
}

int main(int, char * [])
{
    int inputData;
    cout << "Jump height: ";
    cin >> inputData;
    checkForBadInputData(inputData);
    const float maxHeightTime = countMaxHeightTime(inputData);
    cout << "Time, when the maximum height is reached: " << maxHeightTime << std::endl;
    printCurrentHeightAndTime(maxHeightTime);
    
    return 0;
}
